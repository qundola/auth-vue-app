export interface ITodoItem {
  id: String
  userId: String
  title: String
  completed: Boolean
  isFavorite?: Boolean
}
