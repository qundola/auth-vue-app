import axios from 'axios'
import type { AxiosInstance } from 'axios'
import type { ITodoItem } from '../types/ITodoItem'

class TodoListService {
  private apiClient: AxiosInstance

  constructor() {
    this.apiClient = axios.create({
      baseURL: 'https://jsonplaceholder.typicode.com',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
  }

  public async getTodos(): Promise<{ result: ITodoItem[] | null; error: string | null }> {
    let result: ITodoItem[] | null = null
    let error: string | null = null

    try {
      result = (await this.apiClient.get('/todos')).data
    } catch {
      error = 'Problem with connections'
    }

    return { result, error }
  }

  public async createTodo(data: {
    userId: string
    title: string
  }): Promise<{ result: ITodoItem[] | null; error: string | null }> {
    let result: ITodoItem[] | null = null
    let error: string | null = null

    try {
      result = (await this.apiClient.post('/todos', data)).data
    } catch (e) {
      error = 'Problem with connections'
    }

    return { result, error }
  }
}

export default new TodoListService()
