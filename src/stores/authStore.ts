import { defineStore } from 'pinia'
import axios from 'axios'

interface User {
  id: number
  username: string
  phone: string
}

export const useAuthStore = defineStore({
  id: 'auth',

  state: () => ({
    user: null as User | null,
    error: null as string | null
  }),

  actions: {
    async register(username: string, phone: string) {
      this.error = null

      try {
        const { data: users } = await axios.get<User[]>(
          'https://jsonplaceholder.typicode.com/users'
        )

        const user = users.find((user) => user.username === username && user.phone === phone)

        if (user) {
          this.user = user
          window.localStorage.setItem('User', JSON.stringify(user))
          this.error = null
        } else {
          this.error = 'User not found'
        }
      } catch (error) {
        this.error = 'An error occurred during authentication'
      }
    },

    getSavedUser() {
      const localUser = window.localStorage.getItem('User')
      if (localUser) {
        this.user = JSON.parse(localUser)
        return this.user
      }
      return false
    },

    logout() {
      this.user = null
      this.error = null
    }
  }
})
