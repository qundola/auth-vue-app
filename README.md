# Auth Vue App

This project was created for a test assignment to a company NuxGame ❤️‍🔥

## It has been implemented:

- Authorization for the user
- Drawing todolist
- Search todoitem by title
- Filtering by user id
- Filtering by todoitem status
- Creating your own todoitem
- Adaptive layout for correct display on phones


## Stack 💪

- Vue 3
- TypeScript
- Axios
- Vite
- Pinia
- SCSS

I'm very much looking forward to feedback on the work done, I'm always ready for criticism, which will encourage me to develop! 😉
